import * as actions from './actions';

const initialState = {
    id: null,
    email: null
}

const signup = (state, action) => {
    const updatedState = { ...state };

    updatedState.id = action.payload.id;
    //updatedState.email=actions.email;

    return updatedState;
}

const login = (state, action) => {
    const updatedState = { ...state };

    updatedState.id = action.payload.id;
    updatedState.email = action.payload.email;
    updatedState.name = action.payload.name;
    //updatedState.email=actions.email;

    return updatedState;
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actions.AUTH_SIGNUP:
            return signup(state, action);
        case actions.AUTH_LOGIN:
            return login(state, action);
        default:
            return state;
    }

}

export default reducer;