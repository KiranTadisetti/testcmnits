 export const AUTH_LOGIN='AUTH_LOGIN';
 export const AUTH_SIGNUP='AUTH_SIGNUP';

 export const signup=(id)=>{
    return {
        type:AUTH_SIGNUP,
        payload:{
            id:id
        }
    }
 };

 export const login=(id,email,name)=>{
    return {
        type:AUTH_LOGIN,
        payload:{
            id,
            email,
            name
        }
    }
 };