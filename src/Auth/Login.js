import React,{useState} from 'react';
import {connect} from 'react-redux';

import * as actions from '../store/actions';

const Login = props=>{
    const [userInfo, setUserInfo]= useState({
        name:'',
        password:''
        
    });

    const formSubmitHandler=async (event)=>{
        event.preventDefault();

        const url ="https://reqres.in/api/users/2";
        // const options={
        //     body: userInfo
        // }
        const request= new Request(url);
        await fetch(request)
        .then(res=>{
            console.log(res);
            return res.json();
        })
        .then(res=>{
            console.log(res);
            const {id,email,first_name}=res.data;
            props.onLoginsucess(id,email,first_name);
            props.history.push('/');
        })
        .catch(err=>{
            console.log(err);
            alert('Login failed')
        })
        
    }
    const namechangeHandler=(event)=>{
        let newUserInfo={...userInfo};
        newUserInfo.name=event.target.value;
        setUserInfo(newUserInfo);
    }
    const passwordchangeHandler=(event)=>{
        let newUserInfo={...userInfo};
        newUserInfo.password=event.target.value;
        setUserInfo(newUserInfo);
    }
   

    return (
        <div>
        <h1>Login</h1>
            <form onSubmit={formSubmitHandler}>
                <input type='text' placeholder="Enter Name"  onChange={(event)=>namechangeHandler(event)} />
                <input type='password' placeholder="Enter Password"  onChange={passwordchangeHandler}/>
                
                <button >submit</button>
            </form>
        </div>
    )
}
const mapStateToProps=(state)=>{
    return{
        id:state.id,
        name:state.name
    }
}
const mapDispatchToProps=(dispatch)=>{
    return {
        onLoginsucess :(id,email,name)=>dispatch(actions.login(id,email,name))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Login);