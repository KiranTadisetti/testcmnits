import React, { useState } from 'react';
//import Request from 'postman-request';
import validator from 'validator';

import {connect} from 'react-redux';

import * as actions from '../store/actions';

const Signup = (props) => {
    const [userInfo, setUserInfo] = useState({
        name: '',
        password: '',
        email: '',
        job: ''
    });

    const isValidUserInfo=()=>{
        let valid=true;

        if(userInfo.name==null){
            valid=false;
            return valid;
        }
        valid= validator.isEmail(userInfo.email);


        return valid;
    }
    const formSubmitHandler = async (event) => {
        event.preventDefault();
        if (isValidUserInfo()) {
            const url = "https://reqres.in/api/users/";
            const options = {
                method: 'POST',
                body: userInfo
            }
            const request = new Request(url, { ...options });
            await fetch(request)
                .then(res => {
                    console.log(res);
                    return res.json();
                })
                .then(res=>{
                    console.log(res);
                    props.onSignup(res.id);
                    props.history.push('/dashboard');
                })
                .catch(err => {
                    alert(err);
                })
        }
        else{
            alert("invalid userinfo")
        }
    }
    const namechangeHandler = (event) => {
        let newUserInfo = { ...userInfo };
        newUserInfo.name = event.target.value;
        setUserInfo(newUserInfo);
    }
    const passwordchangeHandler = (event) => {
        let newUserInfo = { ...userInfo };
        newUserInfo.password = event.target.value;
        setUserInfo(newUserInfo);
    }
    const jobchangeHandler = (event) => {
        let newUserInfo = { ...userInfo };
        newUserInfo.job = event.target.value;
        setUserInfo(newUserInfo);
    }
    const emailchangeHandler = (event) => {
        let newUserInfo = { ...userInfo };
        newUserInfo.email = event.target.value;
        setUserInfo(newUserInfo);
    }

    return (
        <div>
        <h1>sign-up</h1>
            <form onSubmit={formSubmitHandler}>
                <input type='text' placeholder="Enter Name" onChange={(event) => namechangeHandler(event)} />
                <input type='password' placeholder="Enter Password" onChange={passwordchangeHandler} />
                <input type='text' placeholder="Enter job" onChange={jobchangeHandler} />
                <input type='text' placeholder="Enter Email" onChange={emailchangeHandler} />
                <button >submit</button>
            </form>
        </div>
    )
}

const mapToDispatchProps=(dispatch)=>{
    return {
        onSignup:(id)=>dispatch(actions.signup(id))
    }
    
}


export default connect(null,mapToDispatchProps)(Signup);