import React from 'react';
import {Route,Switch} from 'react-router-dom';

import Login from './Auth/Login';
import Signup from './Auth/Signup';
import Dashboard from './Dashboard';

import './App.css';

function App() {
  let routes=(
    <Switch>
    <Route path="/login" render= {(props:any)=><Login {...props}/>}/>
    <Route path="/signup" render= {(props:any)=><Signup {...props}/>}/>
    <Route path="/" render= {(props:any)=><Dashboard {...props}/>}/>
    </Switch>
    
  )
  return (
    <div className="App">
      
      <a href='/login'>Login</a>
      
      <a href='/signup'>Signup</a>
      <a href='/'>Dashboard</a>
      {routes}
    </div>
  );
}

export default App;
