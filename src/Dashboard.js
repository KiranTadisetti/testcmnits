import React from 'react';
import {connect} from 'react-redux';

const dashboard = props=>{
const isLoggedIn= props.id?true:false;
    return (
        <div>
            <h1> dashboard</h1>
            {isLoggedIn?<p>Welcome {props.name+"\n"+props.email}</p>:<p>Please Login</p>}
        </div>
    )
}

const mapStateToProps=(state)=>{
    return{
        id:state.id,
        name:state.name,
        email:state.email
    }
}
export default connect(mapStateToProps,null)(dashboard);